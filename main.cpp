//# Temat p3
//
// Jako ostatnie zadanie proszę o zaimplementowanie algorytmu MergeSort (w postaci funkcji),
// tak aby było możliwe posortowanie listy liczb.
//
// Listę liczb należy wczytać z pliku.

#include <iostream>
#include <fstream>


using namespace std;

void mergeSort(int *arr, int from, int to) {
    int halfWay;
    if (from < to) {
        halfWay = (from + to) / 2;
        mergeSort(arr, from, halfWay);
        mergeSort(arr, halfWay + 1, to);

        //merge

        int leftSize, rightSize, i, j, k;
        leftSize = halfWay - from + 1;
        rightSize = to - halfWay;
        int L[leftSize], R[rightSize];
        //copy Left
        for (i = 0; i < leftSize; i++) {
            L[i] = arr[from + i];
        }
        //copy Right
        for (j = 0; j < rightSize; j++) {
            R[j] = arr[halfWay + j + 1];
        }
        i = 0, j = 0;
        //put new values into source array
        for (k = from; i < leftSize && j < rightSize; k++) {
            if (L[i] < R[j]) {
                arr[k] = L[i];
                i++;
            } else {
                arr[k] = R[j];
                j++;
            }
        }

        //put remaining elements
        while (i < leftSize) {
            arr[k] = L[i];
            i++;
            k++;
        }
        while (j < rightSize) {
            arr[k] = R[j];
            j++;
            k++;
        }
    }
}

void loadFromFile(int *arr, string filename, int &size) {

    size = 0;
    ifstream file(filename.c_str());

    if (!file.good()){
        cout << "plik nie istnieje" << endl;
        return;
    }

    int temp;
    while (file >> temp) {
        arr[size] = temp;
        size++;
    }

    file.close();

}

int main() {
    int inputArray[100];
    int size;

    loadFromFile(inputArray, "data.txt", size);

    cout << "Input array\n";
    for (int i = 0; i < size; i++) { cout << inputArray[i] << "\t"; }
    cout << "\n\n";

    mergeSort(inputArray, 0, size - 1);

    cout << "Sorted array\n";
    for (int i = 0; i < size; i++) { cout << inputArray[i] << "\t"; }

    return 0;
}
